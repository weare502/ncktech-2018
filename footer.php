<?php if(!is_page_template("page-landing.php")){ ?>
	</div><!-- End Wrapper2 -->
	</div><!-- Container End -->
	<div class="full-width footer-widget">
		<div class="row">
			<?php dynamic_sidebar("Footer"); ?>
		</div>
	</div>
	<footer class="full-width" role="contentinfo">
		<div class="row">
			<div class="large-12 columns">
				<p>NCK Tech is committed to nondiscrimination on the basis of race, color, gender, ethnic or national origin, sex, sexual orientation, gender identity, marital status, religion, age, ancestry, disability, military status, or veteran status in admission or access to, or treatment or employment in, its programs and activities. Further, it is the policy of the college to prohibit harassment (including sexual harassment and sexual violence) of students and employees. Any person having inquiries concerning the college's compliance with the regulations implementing Title VI, Title VII, Title IX, Section 504, and the Americans with Disabilities Act Amendments Act is directed to the Dean of Instructional Services (Section 504/ADA Compliance Officer) or the Dean of Student Services (Title VI, Title VII, &amp; Title IX Compliance Officer) at <a href="tel:1-800-658-4655">1-800-658-4655</a> or PO Box 507, 3033 US Hwy 24, Beloit, KS 67420.</p>

				<?php wp_nav_menu(array('theme_location' => 'utility', 'container' => false, 'menu_class' => 'inline-list')); ?>
			</div>
		</div>
		<div class="row love-reverie">
			<div class="large-12 columns">
				<p style="text-align: left; float: left;">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All rights reserved. <a href="<?php echo get_page_link(344); ?>">Privacy Policy</a></p>
			<!-- 	<p style="text-align: right; float: right;" class="show-for-large-up">Designed and Developed by <a href="http://502mediagroup.com" rel="nofollow">502 Media Group</a></p> -->
			</div>
		</div>
	</footer>
<?php }; ?>
<?php wp_footer(); ?>
<script type="text/javascript">
	//Initialize Foundation
	(function($) {
		//$(document).foundation();
	})(jQuery);

	// Remove Break Tags in Accordion Plugin
	jQuery(document).ready(function(){
		jQuery('.accordion br').remove();
	});

	//Clip shadow from #content area
	jQuery(window).load(function(){
		//init variable
		$content = jQuery('#content');
		//print initial style tag to be modified
		jQuery('<style class="idcontent" type="text/css">#content:before{ clip: rect(0,3000px,'+$content.outerHeight()+'px, -60px);}</style>').appendTo(jQuery('head'));
		//on window resize, replace rule with updated variable
		jQuery(window).resize(function(){
			jQuery('.idcontent').html('#content:before{ clip: rect(0,3000px,'+$content.outerHeight()+'px, -60px);}');
		});
		jQuery('.accordion-title').click(function(){
			function redrawShadow(){
				jQuery('.idcontent').html('#content:before{ clip: rect(0,3000px,'+jQuery('#content').outerHeight()+'px, -60px);}');
			}
			window.setTimeout(redrawShadow, 1000);
			//console.log('accordion-title clicked');
		});

		jQuery('#print-button').attr("href", "javascript:window.print()");
	});
</script>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-9944557-41', 'auto');
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

</script>
<script>(function() {
 var _fbq = window._fbq || (window._fbq = []);
 if (!_fbq.loaded) {
   var fbds = document.createElement('script');
   fbds.async = true;
   fbds.src = '//connect.facebook.net/en_US/fbds.js';
   var s = document.getElementsByTagName('script')[0];
   s.parentNode.insertBefore(fbds, s);
   _fbq.loaded = true;
 }
 _fbq.push(['addPixelId', '292146644306199']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=292146644306199&amp;ev=PixelInitialized" /></noscript>
<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5429dc77564f88ccfe00002d.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
<!-- Hotjar Tracking Code for https://www.ncktc.edu -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:615384,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
</body>
</html>