<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<title><?php wp_title('|', true, 'right'); ?></title>

	<!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Favicon and Feed -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo home_url(); ?>/favicon.ico">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

<style>
.issuuembed { width: 100% !important; height: 90vh !important; max-height: 40rem; } #wp-toolbar.quicklinks { float: none; }
</style>
<?php wp_head(); ?>
<!-- SiteScout Retargeting -->
<script type="text/javascript">var ssaUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'pixel.sitescout.com/iap/41f86be8f9b56000';new Image().src = ssaUrl;</script>
</head>

<body <?php body_class('antialiased'); ?> >
<?php if(!is_page_template("page-landing.php")){ ?>
	<?php do_action( 'tha_body_top' ); ?>
	
	<header id="main-header" class="row" role="navigation">
		<nav class="quicklinks">
			<a href="http://mail.google.com/a/ncktc.edu">Webmail</a>
				<span> - </span> 
			<!-- <a href="https://selfserve.ncktc.edu/SelfService/Home.aspx">Self-Service</a>
				<span> - </span> -->
			<a href="/cams/">CAMS</a>
				<span> - </span> 	
			<a href="<?php echo get_permalink(375); ?>">Contact</a>
	<!-- 			<span> - </span>
			<a href="#">Admin Login</a> -->
			<?php get_search_form(); ?>
		</nav>
	
	<!-- Starting the Top-Bar -->
		<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" /></a><small style="font-family: Arial; font-style: italic; color: #222;">Hands-On Skills. High-Tech Careers.</small></h1>
	    <?php uberMenu_easyIntegrate(); ?>
	<!-- End of Top-Bar -->
	</header>
	
	<!-- Start the main container -->
	<div class="container" role="document">
		<div class="wrapper1">
			<?php if( is_front_page() ){
		    	echo do_shortcode("[metaslider id=59]"); 
			} ?>
		</div>
		<div class="wrapper2">
<?php }; ?>