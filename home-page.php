<?php
/*
Template Name: Home Page
*/
get_header(); 
function custom_excerpt_length( $length ) {
        return 25;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 10 );
?>

<!-- Row for main content area -->
<div class="halfbar row"></div>
<div class="row" id="content" style="padding-top: 2rem;">
	<div class="small-12 large-6 columns" id="video-meta">
		<div class="flex-video" style="margin-top: 1rem;">
			<iframe width="640" height="360" src="//www.youtube.com/embed/70BGKXMGXzI/?rel=0 &wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen ></iframe>
		</div>
		<!-- <div class="row">
			<div class="small-12 center">
				<a href="/apply/" class="button success large" id="apply-button">Apply Now!</a>
			</div>
		</div> -->
		<div class="row">
			<div class="small-12 home-page-content">
				<?php the_field('home_page_content'); ?>
			</div>
		</div>
		<?php 
			$args = array( // Start Featured Event
	        'post_type' => 'featured_students',
			'posts_per_page' => 5	
			);

			// image vars for alt text
			$image = get_field('featured_student_image');
			$alt = $image['alt'];

			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) : ?>
				<div id="slider-wrapper" class="column show-for-large-up" style="opacity: 0;">
					<div id="home-slider">
		   				<?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
			
							<div class="clearfix">
								<img src="<?php the_field('featured_student_image'); ?>" id="image1" class="show-for-large-up left" alt="<?php echo $alt; ?>" />
								<aside class="right">
									<p><?php the_excerpt(); ?></p>
									<a href="<?php the_permalink(); ?>" class="button success large"><?php the_field('featured_student_button_text'); ?></a>
								</aside>
							</div>

						<?php endwhile; ?>
					</div>
				</div>
			<?php endif; wp_reset_postdata(); ?>
 	</div>
	<div class="small-12 large-6 columns" id="meta">
		<div class="row">
			<div class="small-12 large-6 columns">
				<h3>Take a Tour</h3>
				<p>Welcome to NCK Tech! Have a look around and explore all the opportunities available for students at NCK. Take a look at our two unique campuses, offering a wide variety of programs, to help you excel in some seriously exciting career fields!</p>
			</div>
			<div class="small-12 large-6 columns">	
				<h3>Upcoming Events</h3>
			
				<?php 
					$i = 0;
					$EM_Events = EM_Events::get( array(
						'scope'=>'future', 
						'orderby'=>'event_start_date', 
						) );	

					$index = 0;
					foreach ( $EM_Events as $event ){
						if ( $event->event_attributes['display_on_homepage'] == 0){
							unset($EM_Events[$index]);
						}
						$index++;
					}


	   				foreach ( $EM_Events as $EM_Event) :  	
						$i++;				
				?>
				
				
				<hr style="margin: 1rem 0;"/>
				<div style="margin-bottom: 1rem; overflow: hidden;">
					<div class="event-link" style="display: block; color: #333;">
						<time class="icon">
							<em><?php echo $EM_Event->output('#l'); ?></em>
							<strong><?php echo $EM_Event->output('#F'); ?></strong>
							<span><?php echo $EM_Event->output('#d'); ?></span>
						</time>
					
						<h5><a href="<?php echo $EM_Event->output('#_EVENTURL'); ?>"><?php echo $EM_Event->output('#_EVENTNAME'); ?></a><br><small><?php echo $EM_Event->output('#_EVENTTIMES'); ?></small></h5>
						<?php // the_excerpt(); ?><a href="<?php echo $EM_Event->output('#_EVENTURL'); ?>" class="left">More Info...</a><a href="/events" class="right">More Events</a>
					</div>
				</div>
				<?php if ( $i === 2 ) {	break 1; } ?>
				<?php endforeach; wp_reset_postdata(); // End Featured Event ?>
			</div>
		</div>
		<div class="row" style="margin-top: 3rem;">
			<div class="small-12 column">
				<h3><a href="/news/">Recent News</a></h3>
				<hr style="margin: .5rem 0;"/>
			<?php	$args = array( // Start Featured Event
			        'post_type' => 'post',
					'posts_per_page' => 1,
					'cat' => -12	
					); 
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<h4 style="color: black;"><?php the_title(); ?></h4>
				
				<p><?php the_excerpt(); ?><a href="<?php the_permalink(); ?>"> Read More &rarr;</a></p>
			<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div>
</div> <!-- end #content -->
<?php wp_enqueue_script('anyslider', get_stylesheet_directory_uri().'/js/anyslider.min.js' ); ?>
<script type="text/javascript">
	jQuery( document ).ready(function( $ ) {

		function heightonload(){
				var $heightobj = $('#meta');
				var $height = $heightobj.height();
				// console.log($height);
		
				$('#video-meta').css('height', $height);
		}
		var $slider = $('#slider-wrapper');
		
		$("#home-slider").AnySlider();
		
		var $window = $(window);
		$window.load(function(){
			if ( $window.width() > 1024 ){
				heightonload();
				// console.log('window wider than 1024');
			}
			$slider.animate({opacity: 1}, 3000);
		});

		$window.resize(function () {
			if ($window.width() > 1024){
				$height = $('#meta').height();
				$('#video-meta').css('height', $height);
			} else {
				$('#video-meta').css('height', 'auto');
			}
		});

	});
</script>
<style type="text/css">
	#home-slider {
		-ms-touch-action: none;
		overflow: auto;
		position: relative;
		touch-action: none;
	}
	#slider-wrapper {
		position: absolute;
		z-index: 0;
		overflow: hidden;
		bottom: -1px;
		width: 94%;
	}
	.as-nav {
		position: absolute;
		bottom: 5px;
		margin: 0 250px;
		width: 50px;
		font-size: .5em;
	}
	.as-nav a {
		color: rgba(0,0,0,0);
	}
	.as-nav a:after {
		content: '\2B24';
		color: #0064C5;
	}
	.as-nav a.as-active:after { color: #0095DA; }
	.as-next-arrow, .as-prev-arrow { display: none; }
	#home-slider aside.right {
		max-width: 60%;
	}
	#home-slider img.left {
		max-width: 40%;
	}
</style>
<?php get_footer(); ?>