<?php get_header(); ?>
<style type="text/css">
	article .fa:before {
		width: 2rem;
		display: none;
	}
	article ul { 
		list-style-type: none; 
		margin-left: 0;
		text-align: left;
		word-wrap: break-word;
	}
	.small-12.medium-6.panel p {
		font-size: 12px;
		margin-bottom: 0!important;
	}
	.small-12.medium-6.panel h3 {
		text-align: center;
		margin-top: 1rem;
	}
	.small-12.medium-6.panel a { font-weight: normal; }
	@media screen and (min-width: 641px) {
		.small-12.medium-6.panel {
			margin: 0 3.3% 2rem 0;
			min-height: 385px;
		}
		.small-12.medium-6.panel img {
			display: none !important;
		}
		.small-12.medium-6.panel p {
			margin: 0;
		}
		article .fa:before { display: inline-block; }
	}
	@media screen and (min-width: 800px){
		.small-12.large-3.panel {
			width: 23%;
			margin: 0 1% 1rem;
			min-height: 450px;
		}
		.small-12.medium-6.panel img {
			display: block !important;
		}
		.small-12.medium-6.panel > p:nth-of-type(1) {
			margin-bottom: 1.25rem;
		}
	}
/*	@media screen and (max-width: 1024px){
		.small-12.medium-6.panel {
			width: 40%;
		}
	}*/

</style>
<!-- Row for main content area -->
	<div id="content" class="row">
		<div class="small-12 columns" role="main">

		<?php while (have_posts()) : the_post(); ?>

			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<header>
					<h1 class="entry-title center"><?php the_title(); ?></h1>
				</header>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>

				<h2 class="center">Administration</h2>
				<div class="row">
					<div class="large-10 columns large-centered">
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ericburks.png" width="100%" class="show-for-medium-up" alt="Eric Burks" />
							<h3><a href="https://www.ncktc.edu/faculty/eric-burks/">Eric Burks</a></h3>
							<p style="font-weight: bold;">President</p>
							<p>B.S. Kansas State University; M.S. Fort Hays State University</p>
						</div>
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sandragottschalk.png" width="100%" class="show-for-medium-up" alt="Sandra Gottschalk" />

							<h3><a href="https://www.ncktc.edu/faculty/sandra-gottschalk/">Sandra Gottschalk</a></h3>
							<p style="font-weight: bold;">Dean – Hays</p>
							<p>B.S. Nursing Marymount College; M.S. Nursing Education Fort Hays State University</p>
						</div>
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coreyisbell.png" width="100%" class="show-for-medium-up" alt="Corey Isbell" />

							<h3><a href="https://www.ncktc.edu/faculty/corey-isbell/">Corey Isbell</a></h3>
							<p style="font-weight: bold;">Dean of Instructional Services</p>
							<p>B.S. Fort Hays State University; M.S. Kansas State University</p>
						</div>
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/angelprescott.png" width="100%" class="show-for-medium-up" alt="Angel Prescott" />

							<h3><a href="https://www.ncktc.edu/faculty/angel-prescott/">Angel Prescott</a></h3>
							<p style="font-weight: bold;">Dean of Student Services</p>
							<p>B.S. Northwest Missouri State University; M.S. Northwest Missouri State University</p>
						</div>	
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/brandizimmer.png" width="100%" class="show-for-medium-up" alt="Brandi Zimmer" />

							<h3><a href="https://www.ncktc.edu/faculty/brandi-zimmer/">Brandi Zimmer</a></h3>
							<p style="font-weight: bold;">Dean of Administrative Services</p>
							<p>B.B.A. Fort Hays State University</p>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="large-10 columns large-centered">
						<div class="small-12 medium-6 large-2 columns panel">
						<?php // if ( getimagesize(get_stylesheet_directory_uri() . '/img/iandraemel.png')[0] === 250 ) : ?>
							<!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iandraemel.png" width="100%" class="show-for-medium-up" /> -->
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/nophoto.png" width="100%" class="show-for-medium-up" alt="Ian Draemel" />
						<?php //else : <img src="//i.imgur.com/TDwd4Oi.png" width="100%" class="show-for-medium-up" /> ?>


							<h3><a href="http://www.ncktc.edu/faculty/ian-draemel/">Ian Draemel</a></h3>
							<p style="font-weight: bold;">Network Systems Administrator</p>
							<p>B.S. Kansas State University</p>
						</div>
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/nophoto.png" width="100%" class="show-for-medium-up" alt="Michele Unrein" />

							<h3><a href="http://www.ncktc.edu/faculty/michele-unrein/">Michele Unrein</a></h3>
							<p style="font-weight: bold;">Director of Nursing – Hays</p>
							<p>B.S. Nursing, Fort Hays State University; M.S. Nursing, Fort Hays State University</p>
						</div>
						<div class="small-12 medium-6 large-2 columns panel">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/garyodle.png" width="100%" class="show-for-medium-up" alt="Gary Odle" />

							<h3><a href="http://www.ncktc.edu/faculty/gary-odle/">Gary Odle</a></h3>
							<p style="font-weight: bold;">Financial Aid Director</p>
							<p>B.S. Fort Hays State University &amp; M.S. Pittsburg State University</p>
						</div>
						<div class="small-12 medium-6 large-2 columns"> <!-- add panel class -->
							<!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pattiscott.png" width="100%" class="show-for-medium-up" alt=""/>

							<h3><a href="http://www.ncktc.edu/faculty/patti-scott/">Patti Scott</a></h3>
							<p style="font-weight: bold;">Director of Nursing – Beloit</p>
							<p>B.S.N. Fort Hays State University; M.S.N. Fort Hays State University</p> -->
						</div>
						<div class="small-12 medium-6 large-2 columns"><!-- add panel class -->
							<!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/nicolenck.png" width="100%" class="show-for-medium-up" alt="" />
							
							<h3><a href="https://www.ncktc.edu/faculty/nicole-rainey/">Nicole Rainey</a></h3>
							<p style="font-weight: bold;">Director of Marketing / Foundation</p>
							<p>Bachelor of the Arts in History <br/> Ethnic Studies Certification</p> -->
						</div>
					</div>
				</div>
					
				<div class="row" style="margin: 0 auto; text-align: center; ">
				<h4><span class="fa fa-search"></span>Search Faculty/Staff</h4>
					<div class="small-12 medium-6" style="float: none; margin: 0 auto;">
						<input id="filter_input" type="text" placeholder="Type to Filter - Search by Name, Email, or Phone Number" style="margin: 0 auto; float: none;" />
					</div>
				</div>


				<div class="row" style="text-align: center;">
					<div class="small-12 medium-6 columns">
						<h2>Beloit Faculty</h2>
						<ul class="filter">
							<?php 
								add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
								$args = array(
							        'post_type' => 'faculty',
							        'advisors' => 'beloit-faculty',
							        'posts_per_page' => -1, 		
				    			); 
				    			$loop = new WP_Query( $args );
				   				while ( $loop->have_posts() ) : $loop->the_post();
				   				$bio = get_field('faculty_biography');
				   				$first = strstr($bio,"\n",true);
							?>
							<li class="panel">
								<span class="fa fa-user"></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br/>
								<span class="fa fa-envelope">Email:&nbsp;</span><?php the_field('faculty_email');?><br/>
								<span class="fa fa-phone">Phone:&nbsp;</span><a href="tel:<?php the_field('faculty_phone'); ?>"><?php the_field('faculty_phone'); ?></a>
								<?php echo $first; ?>
							</li>
							<?php endwhile; wp_reset_postdata(); remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?>
						</ul>

						<h2>Beloit Staff</h2>
						<ul class="filter">
							<?php
								add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
								$args = array(
							        'post_type' => 'faculty',
							        'advisors' => 'beloit-staff',
							        'posts_per_page' => -1, 			
				    			); 
				    			$loop = new WP_Query( $args );
				   				while ( $loop->have_posts() ) : $loop->the_post();
				   				$bio = get_field('faculty_biography');
				   				$first = strstr($bio,"\n",true);
							?>
							<li class="panel">
								<span class="fa fa-user"></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br/>
								<span class="fa fa-envelope">Email:&nbsp;</span><?php the_field('faculty_email');?><br/>
								<span class="fa fa-phone">Phone:&nbsp;</span><a href="tel:<?php the_field('faculty_phone'); ?>"><?php the_field('faculty_phone'); ?></a>
								<?php echo $first; ?>
							</li>
							<?php endwhile; wp_reset_postdata(); remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?>
						</ul>
					</div><!-- End Column -->

					<div class="small-12 medium-6 columns">
						<h2>Hays Faculty</h2>
						<ul class="filter">
							<?php 
								add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
								$args = array( 
							        'post_type' => 'faculty',
							        'advisors' => 'hays-faculty',
							        'posts_per_page' => -1, 		
				    			); 
				    			$loop = new WP_Query( $args );
				   				while ( $loop->have_posts() ) : $loop->the_post();
				   				$bio = get_field('faculty_biography');
				   				$first = strstr($bio,"\n",true);
							?>
							<li class="panel">
								<span class="fa fa-user"></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br/>
								<span class="fa fa-envelope">Email:&nbsp;</span><?php the_field('faculty_email');?><br/>
								<span class="fa fa-phone">Phone:&nbsp;</span> 	<a href="tel:<?php the_field('faculty_phone'); ?>"><?php the_field('faculty_phone'); ?></a>
								<?php echo $first; ?></li>
							<?php endwhile; wp_reset_postdata(); remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?>
						</ul>

						<h2>Hays Staff</h2>
						<ul class="filter">
							<?php 
								add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
								$args = array( 
							        'post_type' => 'faculty',
							        'advisors' => 'hays-staff',
							        'posts_per_page' => -1,
				    			);
				    			$loop = new WP_Query( $args );
				   				while ( $loop->have_posts() ) : $loop->the_post();
				   				$bio = get_field('faculty_biography');
				   				$first = strstr($bio,"\n",true);
							?>
							<li class="panel">
								<span class="fa fa-user"></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br/>
								<span class="fa fa-envelope">Email:&nbsp;</span><?php the_field('faculty_email');?><br/>
								<span class="fa fa-phone">Phone:&nbsp;</span><a href="tel:<?php the_field('faculty_phone'); ?>"><?php the_field('faculty_phone'); ?></a>
								<?php echo $first; ?>
							</li>
							<?php endwhile; wp_reset_postdata(); remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?>
						</ul>
					</div><!-- End Column -->
				</div><!-- End row -->

			</article>

		<?php endwhile; // End the loop ?>

		</div>
	</div>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/filter.js"></script>
<script type="text/javascript">	
	jQuery(document).ready(function($){
		$(function() {
        	$('#filter_input').fastLiveFilter('.filter');
   		 });
	});
</script>
<?php get_footer(); ?>