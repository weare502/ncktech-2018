<?php get_header(); ?>
<div id="content" class="row">
	<div class="row">
		<div class="small-12 column">
			<!-- <h1>Faculty Member</h1> -->
			<h1 style="text-align: center;"><br/><?php the_title(); ?></h1>
			<h3 style="text-align: center;"><?php the_terms( 0 , 'advisors', '', '&nbsp;-&nbsp;', ''); ?></h3>
		</div>
	</div>

	<div class="row">
		
		<div class="medium-6 columns" style="text-align: center;">

			<?php // image vars
				$image = get_field('faculty_image');
				$alt = $image['alt']; 
			?>

			<img src="<?php echo $image; ?>" alt="<?php echo $alt; ?>" class="faculty-single-image" />
			
		</div>

		<div class="medium-6 columns">
			<?php if ( strlen(get_field('faculty_biography')) > 1 ) : ?>
				<p><?php the_field('faculty_biography'); ?></p>
			<?php endif; ?>

			<h4>Contact <?php the_title(); ?></h4>
			<p>Office: <a href="tel:<?php the_field('faculty_phone'); ?>"><?php the_field('faculty_phone'); ?></a><br/>
			Email: <a href="mailto:<?php the_field('faculty_email'); ?>">Click Here</a></p>
			
		</div>

	</div>

	<div class="row clearfix">
		<br/>
		<!-- <span style="float:left; padding:1rem 0 1rem 1rem;"> <?php add_filter( 'posts_orderby' , 'posts_orderby_lastname' ); previous_post_link_plus( array(/* 'order_by' => 'post_title',*/ 'link' => 'Previous Faculty Member: %title') ); ?> </span>
		<span style="float:right; padding:1rem 1rem 1rem 0;"> <?php next_post_link_plus( array(/* 'order_by' => 'post_title',*/ 'link' => 'Next Faculty Member: %title') ); remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?> </span> -->
		<br/>
	</div>

</div>
<?php get_footer(); ?>