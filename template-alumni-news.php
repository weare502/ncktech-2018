<?php/*
	Template Name: Alumni News
*/
	?>
<?php get_header(); ?>

<!-- Row for main content area -->
	<div id="content" class="row">
	<div class="small-12 medium-8 medium-push-4 column" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
			<header class="title-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>

			<hr />

			<div class="news-entries">
				<?php 
			$args = array( // Start Featured Event
	        'category_name' => 'alumni-news',
			'posts_per_page' => 5,	
			); 
			$loop = new WP_Query( $args );

			if ( $loop->have_posts() ){
				
			while ( $loop->have_posts() ) : $loop->the_post();   	

			get_template_part( 'content', get_post_format() );
			
			endwhile; 
			} else { echo '<p>Sorry, no news to display at this time.</p>'; } wp_reset_postdata(); ?>

	
			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
				</nav>
			<?php } ?>
			
			</div><!-- End .news-entries -->

	<?php endwhile; // End the loop ?>

	</div><!-- End Main -->

	<div class="small-12 medium-4 medium-pull-8 column panel" id="left-sidebar-menu">
		<h2 style="font-weight: normal; color: #bbb; text-align: center;">Alumni</h2>
		<hr />
		<?php wp_nav_menu( array(
			'menu'			=> 'alumni',
			'container_id'	=> 'alumni-menu',
			'fallback_cb'	=> false,
			) 
		); ?>
	</div>
	</div>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/left-menu.js"></script>
<?php get_footer(); ?>

