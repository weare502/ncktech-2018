<?php/*
	Template Name: Online Classes Pages
*/
	?>
<?php get_header(); ?>

<!-- Row for main content area -->
	<div id="content" class="row">
	<div class="small-12 medium-8 medium-push-4 column" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
			<header class="title-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>

				<?php if (have_rows('course_list_sections') ): ?>
					<div class="course-list">
						<?php while ( have_rows('course_list_sections') ) : the_row(); ?>

			        		<h3><?php the_sub_field('heading'); ?></h3>
			        		<table>
			        			<?php if (have_rows('courses') ) : while ( have_rows('courses') ): the_row(); ?>
			        				<?php 
			        					$online = get_sub_field('online');
			        					$individual = get_sub_field('individual');
			        					$course = false; 
			        					if ( $online != false ){
			        						$course = $online;
			        					} elseif ( $individual != false){
			        						$course = $individual;
			        					}
			        					$course_id = $course->ID;	?>
			        				<tr>
			        					<th><?php the_field('course_code', $course_id) ?></th>
			        					<th><a href="<?php echo get_post_permalink($course_id) ?>"><?php echo $course->post_title ?></a></th>
			        					<th>Credits:&nbsp;<?php the_field('course_credits', $course_id) ?></th>
			        				</tr>
			        				<tr>
			        					<td colspan="3"><?php the_field('course_description', $course_id); ?></td>
			        				</tr>
			        			<?php endwhile; endif; ?>
			        		</table>

			        	<?php endwhile; ?>
					</div>
				<?php endif; ?>
				
			</div>
	<?php endwhile; // End the loop ?>

	</div><!-- End Main -->

	<div class="small-12 medium-4 medium-pull-8 column panel" id="left-sidebar-menu">
		<h2 style="font-weight: normal; color: #bbb; text-align: center;">Online Courses</h2>
		<hr />
		<?php wp_nav_menu( array(
			'menu'			=> 'online-classes',
			'container_id'	=> 'online-classes-menu',
			'fallback_cb'	=> false,
			) 
		); ?>
	</div>
	</div>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/left-menu.js"></script>
<?php get_footer(); ?>

